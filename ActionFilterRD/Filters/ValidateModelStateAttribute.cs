﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ActionFilterRD.Filters
{
    [AttributeUsage(AttributeTargets.All)]
    public sealed class ValidateModelStateAttribute:ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var viewData = filterContext.Controller.ViewData;
            if (!viewData.ModelState.IsValid)
            {
                filterContext.Result = new ViewResult
                {
                    ViewData = viewData,
                    TempData = filterContext.Controller.TempData
                };
            }

            base.OnActionExecuting(filterContext);
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);
        }

     }

    public class CusotomAuthorizationFilter : FilterAttribute, IAuthorizationFilter
    {

        public void OnAuthorization(AuthorizationContext filterContext)
        {
            throw new NotImplementedException();
        }
    }



    public class HomeCommand : IValidatableObject
    {
        [Required]
        public string Name { get; set; }

        [Required]
        [Range(18, 100,ErrorMessage = "You must be 18 or above")]
        public int Age { get; set; }

        public string SubscriptionType { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Age < 60 && SubscriptionType == "Gold")
            {
                yield return  new ValidationResult("Sorry, gold subscriptions are available if you are over 60 years old",new[]
                {
                    "SubscriptionType"
                });
            }
        }
    }
}

