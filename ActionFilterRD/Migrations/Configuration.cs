using System.Collections.Generic;
using Models;

namespace ActionFilterRD.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ActionFilterRD.DBContext.Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ActionFilterRD.DBContext.Context context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            List<User> users = new List<User>
            {
                new User
                {
                    UserName = "Payam",
                    UserLastName = "Shoghi",
                    Age = 30
                },

                new User
                {
                    UserName = "Mohammad",
                    UserLastName = "Salman",
                    Age = 20
                }
            };

            foreach (var user in users)
            {
                context.Users.Add(user);
            }
            context.SaveChanges();

            List<Book> books = new List<Book>
            {
                new Book
                {
                    BookAuthor = "Mr.A",
                    BookName = "Sample A",
                    UserId = 1
                },

                new Book
                {
                    BookAuthor = "Mr. B",
                    BookName = "Sample B",
                    UserId = 2
                },
                 new Book
                {
                    BookAuthor = "Mr.C",
                    BookName = "Sample C",
                    UserId = 1
                },
            };

            foreach (var book in books)
            {
                context.Books.Add(book);
            }

            context.SaveChanges();
        }
    }
}
