﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class User
    {
       public int UserId { get; set; }
       [Required(ErrorMessage = "User name is required")]
       public string UserName { get; set; }

       public string UserLastName { get; set; }
       [Range(10,100,ErrorMessage = "Your age has to be above 10")]
       public int Age { get; set; }
       public ICollection<Book> Books { get; set; } 
    }
}
