﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Book
    {
        public int BookId { get; set; }
        public string BookAuthor { get; set; }
        public string BookName { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
    }
}
